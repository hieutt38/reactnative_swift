//
//  NSAttributedStringUtility.swift
//  USGScrollingTabBar
//
//  Created by M.Satori on 16.09.22.
//  Copyright © 2016年 usagimaru. All rights reserved.
//

import UIKit

extension NSAttributedString {
	
	class func attributedString(_ string: String, font: UIFont, color: UIColor, paragraphStyle: NSParagraphStyle) -> NSAttributedString {
		let att = [
			NSAttributedStringKey.font : font,
			NSAttributedStringKey.foregroundColor : color,
			NSAttributedStringKey.paragraphStyle : paragraphStyle
		]
		
		let astr = NSMutableAttributedString(string: string, attributes: att)
		return astr
	}
	
	
    func addAttributes(attributes: [NSAttributedStringKey : Any]) -> NSAttributedString {
        let astr = self.mutableCopy() as! NSMutableAttributedString
        astr.addAttributes(attributes, range: NSMakeRange(0, length))
        return astr as NSAttributedString
    }
}
