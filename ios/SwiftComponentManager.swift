//
//  SwiftComponentManager.swift
//  ReactNativeSwift
//
//  Created by Hieu Tran on 3/31/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

import Foundation
import UIKit

// SwiftComponentManager

@objc(SwiftComponentManager)
class SwiftComponentManager: NSObject {
  
  @objc func helloSwift(_ greeting: String){
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let controller = storyboard.instantiateViewController(withIdentifier: "FirstView") as! FirstViewController
    controller.greeting = greeting
    
    DispatchQueue.main.sync {
      let topRootViewController: UIViewController = (UIApplication.shared.keyWindow?.rootViewController)!
      topRootViewController.present(controller, animated: true, completion: nil)
    }
  }
  
  @objc func iSchollDemo(){
    let storyboard2 = UIStoryboard(name: "Main2", bundle: nil)
    let controller2 = storyboard2.instantiateViewController(withIdentifier: "secondView") as! SecondViewController
    
    DispatchQueue.main.sync {
      let topRootViewController: UIViewController = (UIApplication.shared.keyWindow?.rootViewController)!
      topRootViewController.present(controller2, animated: true, completion: nil)
    }
  }
  
}
