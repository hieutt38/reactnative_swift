//
//  SecondViewController.swift
//  ReactNativeSwift
//
//  Created by Hieu Tran on 3/31/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

import UIKit

class SecondViewController : UIViewController{
  @IBOutlet weak var scrollingTabBar: USGScrollingTabBar!
  @IBOutlet weak var focusView: UIView!
  @IBOutlet weak var collectionView: UICollectionView!
  
  var tabItems = [USGScrollingTabItem]()
  
  var _lastContentOffset: CGPoint = .zero
  fileprivate let reuseIdentifier = "MainCell"
  
  fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
  //    fileprivate let sectionInsets = UIEdgeInsets.zero
  fileprivate let itemsPerRow: CGFloat = 3
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    scrollingTabBar.delegate = self
    scrollingTabBar.tabBarInset = 8
    scrollingTabBar.tabInset = 12
    scrollingTabBar.tabSpacing = 1
    scrollingTabBar.focusVerticalMargin = 4
    scrollingTabBar.setFocusView(focusView)
    
    buildSampleItems()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    view.layoutIfNeeded()
    
    
    let firstPageIndex = 2
    
    focusView.layer.cornerRadius = (scrollingTabBar.height - scrollingTabBar.focusVerticalMargin * 2.0) / 2.0
    //        scrollView.contentSize = CGSize(width: view.width * CGFloat(tabItems.count), height: scrollView.contentSize.height)
    scrollingTabBar.width = view.width
    //        scrollingTabBar.pageWidth = scrollView.width
    scrollingTabBar.reloadTabs(tabItems, indexOf: firstPageIndex)
    //        scrollView.setContentOffset(CGPoint(x: scrollView.width * CGFloat(firstPageIndex), y: scrollView.contentOffset.y), animated: false)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
  }
  
  fileprivate func buildSampleItems() {
    let strings = [
      "Hành lang",
      "Sân chơi",
      "Lớp mầm xanh",
      "Lớp lá úa",
      "Phòng giám hiệu",
      "Phòng ăn",
      "Phòng chịch"
    ]
    
    let font = UIFont.systemFont(ofSize: 13)
    let color = UIColor.gray
    let highlightedColor = UIColor.blue
    let selectedColor = UIColor.blue
    
    let highlightedAttributes = [
      NSAttributedStringKey.foregroundColor : highlightedColor
    ]
    let selectedAttributes = [
      NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 13),
      NSAttributedStringKey.foregroundColor : selectedColor
    ]
    
    let paragraph = NSMutableParagraphStyle()
    paragraph.alignment = .center
    paragraph.lineBreakMode = .byTruncatingTail
    
    for str in strings {
      let normalString = NSAttributedString.attributedString(str,
                                                             font: font,
                                                             color: color,
                                                             paragraphStyle: paragraph)
      
      let tabItem = USGScrollingTabItem()
      tabItem.normalString = normalString
      tabItem.highlightedString = normalString.addAttributes(attributes: highlightedAttributes)
      tabItem.selectedString = normalString.addAttributes(attributes: selectedAttributes)
      
      tabItems.append(tabItem)
    }
  }
}

extension SecondViewController: USGScrollingTabBarDelegate {
  
  func tabBar(_ tabBar: USGScrollingTabBar, didSelectTabAt index: Int) {
    print("tab \(index) selected")
    //        scrollView.setContentOffset(CGPoint(x: scrollView.width * CGFloat(index), y: scrollView.contentOffset.y), animated: true)
  }
}

extension SecondViewController: UIScrollViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if (abs(_lastContentOffset.x - scrollView.contentOffset.x) > abs(_lastContentOffset.y - scrollView.contentOffset.y)) {
      scrollingTabBar.enabled = !scrollView.isTracking;
      
      if (scrollView.isTracking || scrollView.isDecelerating) {
        scrollingTabBar.scrollToOffset(scrollView.contentOffset.x)
      }
    }
  }
  
  func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
    //        scrollingTabBar.stopScrollDeceleration()
    _lastContentOffset = scrollView.contentOffset;
    scrollingTabBar.enabled = false
  }
  
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    scrollingTabBar.enabled = true
    print(scrollView.contentOffset.x)
  }
  
}
extension SecondViewController: UICollectionViewDelegate, UICollectionViewDataSource{
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      numberOfItemsInSection section: Int) -> Int {
    return 7
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MainCell
    cell.mainView.layer.cornerRadius = 5
    cell.lblValue.text = "\(100 - indexPath.row)%"
    return cell
  }
}

extension SecondViewController: UICollectionViewDelegateFlowLayout{
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    let paddingSpace = CGFloat(5)//sectionInsets.left * (itemsPerRow + 1)
    print(paddingSpace)
    let availableWidth = view.frame.width
    print(availableWidth)
    let widthPerItem = availableWidth / itemsPerRow - paddingSpace * 3
    print(widthPerItem)
    
    return CGSize(width: widthPerItem, height: widthPerItem * 1.2 )
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      insetForSectionAt section: Int) -> UIEdgeInsets {
    return sectionInsets
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return sectionInsets.left
  }
}
